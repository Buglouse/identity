#!/usr/bin/env python3
#
# Identity Manager (Passwords/Authentications)
#
# License
#
import os
import sys
import logging
import sqlite3
import re
import time
import datetime

import pdb

DELIM = ';'
VERBOSITY = 0
# Logging
LOGGER = logging.getLogger()
LOGINF = LOGGER.info
LOGDBG = LOGGER.debug
LOGERR = LOGGER.error

# Create Database
#from sqlite import db

# user:pkey@uri.tld/login_path;tags
# uri, user, pkey, ansr, mail, tags, stamp
class Identity:
    """
    """
    CRYPTEXT = '.gpg'
    DBFILE = 'identity.db'

    def __init__(self, db_path=None):
        self.path = self.set_path(db_path)
        self.conn, self.cur = self.init()
        pass

    def set_path(self, db_path=None):
        """Determine path. Create directory.

        From:
        $XDG_DATA_HOME
        %APPDATA%
        $HOME
        $PWD

        Returns
        -------
        dir
            Path to database directory.
        file
            Database filename.
        """

        if db_path is not None:
            db_path = os.path.split(db_path)
            dir = db_path[0]
            file = db_path[1]
        else:
            file = self.DBFILE
            for home in os.environ.get('XDG_DATA_HOME'), os.environ.get('HOME'), os.environ.get('APPDATA'):
                if home is not None:
                    dir = os.path.join(home, '.identity')
                    break
                else:
                    dir = os.path.abspath('.')

        try:
            if not os.path.exists(dir):
                os.makedirs(dir)
        except Exception as ex:
            LOGERR(ex)
            os._exit(1)

        return os.path.join(dir, file)

    def tags(self, tags=None):
        """Format and get strings from tokens.

        Parameters
        ----------
        tags : list, optional
            List of tags to parse. Default is empty list.

        Returns
        -------
        str
            Comma-delimited string of tags.
        DELIM : str
            If no tags, returns the delimiter.
        None
            If tags is None.
        """

        if tags is None:
            return None
        if not tags or len(tags) < 1 or not tags[0]:
            return DELIM

        rtn = DELIM
        tstr = ' '.join(tags)
        mark = tstr.find(DELIM)

        while mark >= 0:
            tokn = tstr[0:mark]
            tstr = tstr[mark + 1:]
            mark = tstr.find(DELIM)
            tokn = tokn.strip()
            if tokn == '':
                continue
            rtn += tokn + DELIM

        tstr = tstr.strip()
        if tstr != '':
            rtn += tstr + DELIM

        LOGDBG('keywords: %s', rtn)
        LOGDBG('tags: [%s]', rtn)

        if rtn == DELIM:
            return rtn
        ortn = rtn.lower().strip(DELIM).split(DELIM)
        urtn = sorted(set(ortn))
        return delim_wrap(DELIM.join(urtn))
    def tags_append(self, idx, tags):
        if tags is None or tags == DELIM:
            return True
        self.cur.execute('SELECT id, tags FROM identity WHERE id = ? LIMIT 1', (idx,))
        rtn = self.cur.fetchall()
        if rtn:
            tags = rnt[6] + tags[1:]
            tags = self.tags([tags])
            return tags
    def tags_delete(self, idx, tags):
        if tags is None or tags == DELIM:
            return True
        dels = tags.strip(DELIM).split(DELIM)

        self.cur.execute('SELECT id, tags FROM identity WHERE id = ? LIMIT 1', (idx,))
        rtn = self.cur.fetchall()
        if rtn:
            for tag in dels:
                tags = tags.replace(self.tags(tags), DELIM)
            return tags
    def id_max(self):
        self.cur.execute('SELECT MAX(id) from identity')
        rtn = self.cur.fetchall()
        return rtn[0][0]
    def print_row(self, idx=0):
        if idx < 0:
            id = self.id_max()
            if id == -1:
                LOGERR('Empty Database')
                return False
        elif idx != 0:
            try:
                qury = 'SELECT * FROM identity WHERE id = ? LIMIT 1'
                self.cur.execute(qury, (idx,))
                rtn = self.cur.fetchall()
                if not rtn:
                    LOGERR('No matching index at %d', idx)
                    return False
            except IndexError:
                LOGER('No matching index at %d', idx)
                return False
        else: # All
            self.cur.execute('SELECT * FROM identity')
            rtn = self.cur.fetchall()

        if not rtn:
            LOGER('0 records')
            return True
        return True
    def init(self):
        try:
            conn = sqlite3.connect(self.path, check_same_thread=False)
            conn.create_function('REGEXP', 2, regexp)
            cur = conn.cursor()
            cur.execute('CREATE TABLE if not exists identity ('
                        'id integer PRIMARY KEY, '
                        'uri text NOT NULL, '
                        'user text NOT NULL, '
                        'pkey text DEFAULT NULL, '
                        'ansr text DEFAULT \'\', '
                        'mail text DEFAULT \'\', '
                        'tags text DEAFULT \'\', '
                        'stamp integer NOT NULL)')
            conn.commit()
            LOGINF("Database Generate: %s", self.path)
        except Exception as e:
            LOGERR('Database Generate: %s', e)
            sys.exit(1)
        return (conn, cur)
    def id(self, idx):
        """Get an entry by index.
        Parameters
        ----------
        index : integer
            DB index of record.

        Returns
        -------
        seq or None
            Record data or None if index not found.
        """
        self.cur.execute('SELECT id FROM identity WHERE id = ? LIMIT 1', (idx,))
        rtn = self.cur.fetchall()
        if rtn:
            return rtn[0]
    def uri(self, uri):
        self.cur.execute('SELECT * FROM identity WHERE uri = ?', (uri,))
        rtn = self.cur.fetchall()
        if rtn:
            return rtn
    def user(self, user):
        self.cur.execute('SELECT * FROM identity WHERE user = ?', (user,))
        rtn = self.cur.fetchall()
        if rtn:
            return rtn
    def uri_user(self, uri, user):
        self.cur.execute('SELECT * FROM identity WHERE uri = ? AND user = ?', (uri, user,))
        rtn = self.cur.fetchall()
        if rtn:
            return rtn[0]
    def get_user(self, uri, user):
        if not uri or not user or uri == '' or user == '':
            LOGERR('Invalid URI/User')
            return -1
        eid = self.id(uri)
        if eid != -1:
            LOGERR('URI [%s]/Name [%s] already exists at index %s', uri, user, eid)
            return -1
    def create(self, uri, user, pkey=None, ansr=None, mail=None, tags=None):
        id = self.uri_user(uri, user)
        if id is not None:
            LOGERR('Entry [%s/%s] already exists at index %d', uri, user, id[0])
            return -1

        tags = delim_wrap(tags)
        stamp = datetime.date.fromtimestamp(time.time())

        try:
            qry = 'INSERT INTO identity(uri, user, pkey, ansr, mail, tags, stamp) VALUES (?, ?, ?, ?, ?, ?, ?)'
            self.cur.execute(qry, (uri, user, pkey, ansr, mail, tags, stamp))
            #LOGINF = self.list(self.cur.lastrowid)
            return self.cur.lastrowid
        except Exception as ex:
            LOGERR('Creating Entry: %s', ex)
            return -1
    def update(self, idx, uri=None, user=None, pkey=None, \
               ansr=None, mail=None, tags=None):
        id = self.uri_user(uri, user)
        if idx is None:
            LOGERR('Entry does not exist')
            return -1

        args = []
        qury = 'UPDATE identity SET'
        if uri:
            qury += ' uri = ?,'
            args += (uri,)
        if user:
            qury += ' user = ?,'
            args += (user,)
        if pkey:
            qury += ' pkey = ?,'
            args += (pkey,)
        if ansr:
            qury += ' ansr = ?,'
            args += (ansr,)
        if mail:
            qury += ' mail = ?'
            args += (ansr,)
        if tags:
            if tags.startswith('+,'):
                rtn = self.tags_append(idx, tags[1:])
            elif tags.startswith('-,'):
                rtn = self.tags_delete(idx, tags[1:])
            else:
                tags = self.tags(tags)
            qury += ' tags = ?,'
            args += (tags,)
        if len(args) == 0:
            return False
        qury = qury[:-1] + ' WHERE id = ?'
        args += (idx,)

        LOGDBG('update qury: "%s", args: "%s"', qury, args)
        try:
            self.cur.execute(qury, args)
            self.conn.commit()
            if self.cur.rowcount:
                self.print_row(idx)
            if self.cur.rowcount == 0:
                LOGERR('No matching index %d', idx)
                return False
        except sqlite3.OperationalError as ex:
            LOGERR(ex)
            return False
        return True

def delim_wrap(tokn):
    """ Delim Wrap
    Return
    ------
    """
    if tokn is None or tokn.strip() == '':
        return DELIM
    if tokn[0] != DELIM:
        tokn = DELIM + tokn
    if tokn[-1] != DELIM:
        tokn = tokn + DELIM
    return tokn

def regexp(expr, item):
    """ Rexexp
    Return
    ------
    """
    if expr is None or item is None:
        LOGDBG('expr: [%s], item: [%s]', expr, item)
        return False
    return re.search(expr, item, re.IGNORECASE) is not None


def main():
    """ Main
    """
    db = Identity()
    exists_db = os.path.exists(db.path)
    exists_db_crypt = os.path.exists(db.path + db.CRYPTEXT)
    if exists_db_crypt and not exists_db:
        LOGERR('Database Encrypted')
        sys.exit(1)
    elif not exists_db:
        print('Generating Database: %s\n' % db.path)
        db.init(db)

# If crypt, decrypt

# Create/Read/Update/Delete Entry
# Encrypt Database
# Decrypt Database

if __name__ == '__main__':
    main()
