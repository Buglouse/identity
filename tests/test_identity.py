#!/usr/bin/env python3
#
# Unit test
#
import os
import unittest
import sqlite3
import datetime
import time
from genericpath import exists
from tempfile import TemporaryDirectory

from identity import Identity

TEMP_PATH_OBJ = TemporaryDirectory(prefix='identitytest_')
TEMP_PATH = TEMP_PATH_OBJ.name
TEMP_DB_DIR = os.path.join(TEMP_PATH, '.identity')
TEMP_DB_PATH = os.path.join(TEMP_DB_DIR, 'identity.db')

TEST_IDS = [
    ['test.com',
     'user',
     'pass',
     'long answer',
     'my.email@email.com',
     Identity.tags(['test,test2'])],
    ['test2.com',
     'user2',
     'pass2',
     'long answer with more',
     'email2@email.com',
     Identity.tags(['testing'])],
]

class TestIdentity(unittest.TestCase):
    def setUp(self):
        os.environ['XDG_DATA_HOME'] = TEMP_PATH
        if exists(TEMP_DB_PATH):
            os.remove(TEMP_DB_PATH)

        self.ids = TEST_IDS
        self.db = Identity(TEMP_DB_PATH)

    def tearDown(self):
        os.environ['XDG_DATA_HOME'] = TEMP_PATH

    def test_path(self):
        path_expected = TEMP_DB_PATH
        path_local_expected = os.path.join(os.path.expanduser('~'), '.identity', 'identity.db')
        path_relative_expected = os.path.join(os.path.abspath('.'), 'identity.db')

        self.assertEqual(path_expected, Identity(TEMP_DB_PATH).path)

        os.environ.pop('XDG_DATA_HOME')
        self.assertEqual(path_local_expected, Identity().path)

        defaults = {}
        for var in ['HOME', 'HOMEPATH', 'HOMEDIR']:
            try:
                defaults[var] = os.environ.pop(var)
            except KeyError:
                pass
            self.assertEqual(path_relative_expected, Identity().path)
        for key, value in list(defaults.items()):
            os.environ[key] = value

    def test_tags(self):
        tag = self.db.tags(None)
        self.assertIsNone(tag)
        tag = self.db.tags([])
        self.assertEqual(tag, ";")
        tag = self.db.tags([";;;;;"])
        self.assertEqual(tag, ";")
        tag = self.db.tags(["z_tag;a_tag;n_tag"])
        self.assertEqual(tag, ";a_tag;n_tag;z_tag;")
        tag = self.db.tags([" a tag; ;   ;  ;\t;\n;\r;\x0b;\x0c"])
        self.assertEqual(tag, ";a tag;")
        tag = self.db.tags(["tag;tag; tag;    tag;tag ; tag "])
        self.assertEqual(tag, ";tag;")
        tag = self.db.tags(["\"tag\";\'tag\';tag"])
        self.assertEqual(tag, ";\"tag\";\'tag\';tag;")
        tag = self.db.tags([";;z_tag; a tag ;\t;;;   ;n_tag;n_tag;a_tag; \na tag  ;\r; \"a_tag\""])
        self.assertEqual(tag, ";\"a_tag\";a tag;a_tag;n_tag;z_tag;")
    def test_init(self):
        if exists(TEMP_DB_PATH):
            os.remove(TEMP_DB_PATH)
            self.assertIs(False, exists(TEMP_DB_PATH))

        conn, curr = self.db.init()
        self.assertIsInstance(conn, sqlite3.Connection)
        self.assertIsInstance(curr, sqlite3.Cursor)
        self.assertIs(True, exists(TEMP_DB_PATH))
        curr.close()
        conn.close()
    def test_create(self):
        for id in self.ids:
            self.db.create(*id)
            idx = self.db.uri_user(self.ids[0][0], self.ids[0][1])
            db_idx = self.db.id(idx[0])
            self.assertIsNotNone(db_idx)
            for pair in zip(db_idx[1:], id):
                self.assertEqual(*pair)
        self.assertEqual(-1, self.db.create(*self.ids[0]))
    def test_update(self):
        self.db.create(*self.ids[0])
        idx = self.db.uri_user(self.ids[0][0], self.ids[0][1])
        rtn = self.db.update(idx[0], "update.com")
        self.assertTrue(rtn)

    def test_id(self):
        id = self.db.uri_user(self.ids[0][0], self.ids[0][1])
        self.assertEqual(id, self.db.id(0))
    def test_rec_check(self):
        for item in self.ids:
            self.db.create(*item)

if __name__ == '__main__':
    unittest.main()
