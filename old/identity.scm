(module identity
    *
  (import scheme chicken)
  (use data-structures files extras utils posix)
  (use db)

  (define id-table 'identity)
  (define id-db (make-parameter `((,id-table ("id INTEGER PRIMARY KEY"
                                              "uri TEXT NOT NULL COLLATE NOCASE"
                                              "user TEXT NOT NULL"
                                              "pass TEXT DEFAULT NULL"
                                              "stamp INTEGER NOT NULL"
                                              ;;"UNIQUE(uri)"
                                              )))))
  ;; Making other kinds of table schema changes (alter table)
  (define id-key (make-parameter "fake.user@personal.org"))
  (define random-length 18)
  (define id-name (make-parameter
                   (or (get-environment-variable "AUTH_DB")
                       "identity.db")))
  (define id-path (make-parameter ""))
  (define (id-init)
    (let ((d (or (get-environment-variable "DIR_AUTH")
                 (get-environment-variable "DIR_DATA")
                 (get-environment-variable "XDG_DATA_HOME")))
          (c (or (get-environment-variable "DIR_CACHE")
                 (get-environment-variable "XDG_CACHE_HOME")
                 (get-environment-variable "TEMPDIR"))))
      (id-path (make-absolute-pathname d (id-name)))
      (db-path (make-absolute-pathname c (id-name))))
    (db-table (id-db)))

  (define (init)
    (db-create id-table))
  (define (entry kv)
    (set! kv (append kv `((stamp . ,(current-seconds)))))
    (db-insert id-table kv))
  (define (update k v)
    (set! v (append v `((stamp . ,(current-seconds)))))
    (db-update id-table k v))
  (define (view #!optional kv)
    (db-select id-table kv))
  (define (delete k)
    (db-delete id-table k))
  (define (count)
    (db-count id-table))
  (define (type file)
    ;;file: PGP RSA encrypted session key - keyid: 9E5924CB 5D87D3FB RSA (Encrypt or Sign) 2048b .
    ;; Follow Symlinks
    (cadr (string-split (call-with-input-pipe (conc "file --dereference " file) read-all))))
  (define (decrypt)
    (and (file-exists? (id-path))
         (let ((t (type (id-path))))
           (and (cond
                 ((string=? "PGP" t)
                  (system (conc "gpg --trust-model always --quiet --yes "
                                " --output " (db-path)
                                " --decrypt " (id-path)))))
                (file-exists? (db-path))))))
  (define (encrypt)
    (and (system (conc "gpg --trust-model always --quiet --batch --yes "
                       " --output " (id-path)
                       " --recipient '" (id-key) "'"
                       " --encrypt " (db-path)))
         (id-path)))
  (define (environment)
    (map (lambda (e) (printf "Table:  ~S~%" (car e))
                 (map (lambda (ee) (printf "\t\t~S~%" ee)) (cadr e)))
         (id-db))
    (printf "Path:   \t~S~%" (id-path))
    (printf "Cache:  \t~S~%" (db-path))
    (printf "GPG:    \t~S~%" (id-key))
    (printf "Random Length: \t~S~%" random-length)
    )
  (define (id-random len)
    (let ((c "ABCDEFGHIJKLMNOPQRSTUVWXYZabcedfhijklmnopqrstuvwxyz0123456789!@#$%"))
      (if (= 0 len)
          '()
          (cons (string-ref c (random (- (string-length c) 1))) (id-random (- len 1))))))
  )
